Ajilon Developer Meeting #1 - Sample Node.Js Application
===

## Overview
Quick sample application build for the Ajies developer meetings.
Demonstrates Node.JS and some packages including connect, express, socket-io

## Tests

mocha Tests/accountHelper_spec.js -r should -R spec