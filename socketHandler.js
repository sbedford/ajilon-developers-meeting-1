module.exports.initialise = function(app, sessionStore){
	'use strict';

	var io = require('socket.io').listen(app);
	
	var connectedPeople = [];

	var generateKey = function (clientSocket) {
	    return clientSocket.handshake.address.address + ":" + clientSocket.handshake.address.port;
	};

	var parseCookie = require('connect').utils.parseCookie;
	var Session = require('connect').middleware.session.Session;
	
	// assuming io is the Socket.IO server object
	io.configure(function () { 
	  io.set("transports", ["xhr-polling"]); 
	  io.set("polling duration", 10); 
	});

	io.set('authorization', function (data, accept) {
	    // check if there's a cookie header
	    if (data.headers.cookie) {
	        // if there is, parse the cookie
	        data.cookie = parseCookie(data.headers.cookie);
	        // note that you will need to use the same key to grad the
	        // session id, as you specified in the Express setup.
	        data.sessionID = data.cookie['adn.sid'];
	        sessionStore.get(data.sessionID, function (err, session) {
	            if (err || !session) {
	              console.log('Got error: ' + err + ' ' + session);
	                // if we cannot grab a session, turn down the connection
	                accept('Error', false);
	            } else {
	                // save the session data and accept the connection
	                data.session = new Session(data, session);
	                console.log('Got session ' + JSON.stringify(session));
	                accept(null, true);
	            }
	          });
	    } else {
	       // if there isn't, turn down the connection with a message
	       // and leave the function.
	       return accept('No cookie transmitted.', false);
	    }
	    // accept the incoming connection
	    accept(null, true);
	});

	io.sockets.on('connection', function (clientSocket) {

	  var hs = clientSocket.handshake;
	  var nickName = hs.session.nickName;
	  var address = hs.address;

	  console.log("New connection from " + generateKey(clientSocket) + 'for user ' + nickName);

	  // check if the sockets nickName exists in our connected users array
	  var nameExists = false;
	  for (var i=0; i<connectedPeople.length; i++){
	    if (connectedPeople[i] === nickName){
	      nameExists = true; 
	      break;
	    }
	  }

	  if (!nameExists)
	    connectedPeople.push(nickName);

	  clientSocket.emit('updatepeople', connectedPeople);
	  clientSocket.broadcast.emit('updatepeople', connectedPeople);

	  clientSocket.on('sendMessage', function (message) {
	    var nickName = clientSocket.handshake.session.nickName;
	    console.log('in sendMessage with message [' + message + '] from [' + nickName + ']');
	    var timestamp = new Date();
	    timestamp = timestamp.getHours() + ":" + timestamp.getMinutes();
	    
	    clientSocket.broadcast.emit('newmessage', nickName, timestamp, message);
	    clientSocket.emit('newmessage', nickName, timestamp, message);
	  
	  });

	  io.sockets.on('disconnect', function(clientSocket){
	    console.log('A socket from nickName ' + clientSocket.handshake.session.nickName + ' disconnected!');

	    var index = 0;
	    for (var i=0; i<connectedPeople.length; i++){
	      if (connectedPeople[i] === nickName){
	        index=i;
	        break;
	      }
	    }
	    connectedPeople.splice(index,1);
	    clientSocket.broadcast.emit('updatepeople', connectedPeople);
	  });
	});

};