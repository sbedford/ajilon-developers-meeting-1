var httpServer = require('./httpServer'),
    socketHandler = require('./socketHandler'),
    app = httpServer.server;

app.listen(process.env.PORT || 8080);

console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);

// start the socket-io process
socketHandler.initialise(app, httpServer.sessionStore);