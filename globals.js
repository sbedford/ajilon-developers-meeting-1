'use strict';
module.exports.session = {
	key : 'adn.sid',
	secret : 'ajiolondevelopernetwork1234',
	mongourl: 'mongodb://ajisession:ajisession@ds031637.mongolab.com:31637/session'
};

module.exports.viewEngine = {
	viewDirectory : 'views',
	engine : 'jade',
	publicDirectory : 'public'
};

module.exports.mongo = {
	connectionString : 'mongodb://ajidev:ajidev@ds031477.mongolab.com:31477/ajilondevelopersnetwork'
};