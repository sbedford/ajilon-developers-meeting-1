module.exports = function(app)
{
	'use strict';
	
	var developer = require('../models/developer.js');

	function validateLogin(req,res,next) {

		developer.login(req.body.email, req.body.password, function(developer){

			console.log('in success callback with developer ' + developer);			

			req.isValid = developer != null;
			if (developer != null) {
				req.session.authorized = true;
				req.session.nickName = developer.nickName;
			}
			next();

		}, function(err) {

			req.isValid = false;
			req.errorMsg = 'Email and password are invalid';
			next();

		});
	}

	app.get('/account/login', function(req,res) {
		res.render('login', { title: 'Login', layout: "layout.jade"});
	});

	app.post('/account/login', validateLogin, function(req,res) {
		
		if (req.session.authorized)
			res.redirect('/');
		else
			res.render('login', { title: 'Login', isValid: false, errorMsg: req.errorMsg })
	});

	app.get('/account/logout', function(req, res){
		req.session.destroy();
		res.redirect('/account/login');
	});
}