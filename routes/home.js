module.exports = function(app) {
	'use strict';
	
	app.get('/', function(req, res){
		if (!req.session.authorized)
			res.redirect('/account/login');
		else
	  		res.render('index', { title: 'Ajilon Developers Network - Powered by NodeJS', nickName: req.session.nickName })
	});
}