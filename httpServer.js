/**
 * Module dependencies.
 */
var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    mongoStore = require('connect-mongo'),
    connect = require('connect'),
    globals = require('./globals'),
    socketHandler = require('./socketHandler');

var app = module.exports = express.createServer();

var sessionStore = new mongoStore( { url: globals.session.mongourl  });

// Configuration
app.configure(function(){
  app.set('views', globals.viewEngine.viewDirectory);
  app.set('view engine', globals.viewEngine.engine);
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({ store: sessionStore, secret: globals.session.secret, key : globals.session.key }));
  app.use(app.router);
  app.use(express.static(globals.viewEngine.publicDirectory));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Read all controllers from disk and require() them
var controllersPath = path.join(__dirname, "routes");
fs.readdirSync(controllersPath).map(function (controllerName) {
    require('./routes/' + controllerName)(app);
});

module.exports.sessionStore = sessionStore;
module.exports.server = app;