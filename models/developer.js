var developer = function() {
	
	var mongoose = require('mongoose');
	var schema = mongoose.Schema;

	var developerSchema = new schema({
		email : {type : String, index: { unique: true, required : true } },
		password : String,
		nickName : String
	});

	var model = mongoose.model('users', developerSchema);

	mongoose.connect(require('../globals').mongo.connectionString);

	var _login = function(_email, _password, success, failure){

		'use strict';

		model.findOne({ email : _email, password : new Buffer(_password).toString('base64')}, function(err, user)
		{
			if (err){
				failure(err);
			} else {
				var valid = user != undefined;
				var nickName = valid ? user.nickName: null;

				success(user);
			}
		});

	};

	return {
		login : _login
	};

}();

module.exports = developer;