var assert = require('assert');

describe('developer', function(){

	var email = 'sean.bedford@ajilon.com';
	var password = '@jilon01';
	var expected_nickName = 'Sean Bedford';
	var sut = require('../models/developer');

	it('Valid email and password provides developer object', function(done){
		sut.login(email, password, function(user) {  
			user.should.not.equal(null);
			user.nickName.should.equal('Sean Bedford');
			done();
		}, function(error){
			assert(false);
			done();
		});
	});

	it('Invalid email should not provide user object', function(done){
		sut.login('foo@lol.com', 'wrong password', function(user) {  
			assert(user === null, "user should be null");
			done();
		}, function(error){
			assert(false);
			done();
		});
	});

	it('Valid email with invalid password should not provide user object', function(done){
		sut.login(email, 'wrong password', function(user) {  
			assert(user === null, "user should be null");
			done();
		}, function(error){
			assert(false);
			done();
		});
	});
});
